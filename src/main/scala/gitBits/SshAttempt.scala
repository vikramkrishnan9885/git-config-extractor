package gitBits

import java.io.File

import org.eclipse.jgit.transport.{JschConfigSessionFactory, OpenSshConfig, SshSessionFactory, SshTransport, Transport}
import com.jcraft.jsch.{JSch, Session}
import org.eclipse.jgit.api.{Git, TransportConfigCallback}
import org.eclipse.jgit.util.FS

// Jcsh has some issues that have been resolved in this program
// please see the following links:
// https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent
// https://stackoverflow.com/questions/53134212/invalid-privatekey-when-using-jsch
/**
 * Last login: Wed Dec  9 12:38:58 on ttys000
 * vikramkrishnan@C02D75HAMD6M ~ % cd ~/.ssh
 * vikramkrishnan@C02D75HAMD6M .ssh % ls
 * config		id_rsa		id_rsa.pub	known_hosts
 * vikramkrishnan@C02D75HAMD6M .ssh % atom id_rsa
 * vikramkrishnan@C02D75HAMD6M .ssh % ssh-keygen -t rsa
 * Generating public/private rsa key pair.
 * Enter file in which to save the key (/Users/vikramkrishnan/.ssh/id_rsa):
 * /Users/vikramkrishnan/.ssh/id_rsa already exists.
 * Overwrite (y/n)? ^C
 * vikramkrishnan@C02D75HAMD6M .ssh % ssh-keygen -t rsa
 * Generating public/private rsa key pair.
 * Enter file in which to save the key (/Users/vikramkrishnan/.ssh/id_rsa): id_rsa_1
 * Enter passphrase (empty for no passphrase):
 * Enter same passphrase again:
 * Your identification has been saved in id_rsa_1.
 * Your public key has been saved in id_rsa_1.pub.
 * The key fingerprint is:
 * SHA256:JO1pwCWhlL45cwnWH4cnhKTaQJL2nH1oInkx9nRmpvQ vikramkrishnan@C02D75HAMD6M
 * The key's randomart image is:
 * +---[RSA 3072]----+
 * |... .o+o.        |
 * |.+ =.*.O.        |
 * |. *.@.%.o.       |
 * | o X+*.E+.o      |
 * |  +.++.oS=       |
 * |    = o..        |
 * |     +           |
 * |                 |
 * |                 |
 * +----[SHA256]-----+
 * vikramkrishnan@C02D75HAMD6M .ssh % atom id_rsa_1
 * vikramkrishnan@C02D75HAMD6M .ssh % ssh-keygen -t rsa -m PEM
 * Generating public/private rsa key pair.
 * Enter file in which to save the key (/Users/vikramkrishnan/.ssh/id_rsa): id_rsa_1
 * id_rsa_1 already exists.
 * Overwrite (y/n)? y
 * Enter passphrase (empty for no passphrase):
 * Enter same passphrase again:
 * Your identification has been saved in id_rsa_1.
 * Your public key has been saved in id_rsa_1.pub.
 * The key fingerprint is:
 * SHA256:rw5QsUC6ySLaaYPAxGq3XlyIkVI/vcU2bwEgO4mKzVo vikramkrishnan@C02D75HAMD6M
 * The key's randomart image is:
 * +---[RSA 3072]----+
 * |   oo.....       |
 * |. ..+.=o. .      |
 * | +.+ *o. = .     |
 * |=+ooo.+ + o .    |
 * |*+Eo.. oS  o     |
 * |=*..o..  ..      |
 * |+ =. o.   .      |
 * | ....  . .       |
 * |   .   .o        |
 * +----[SHA256]-----+
 * vikramkrishnan@C02D75HAMD6M .ssh % atom id_rsa_1
 * vikramkrishnan@C02D75HAMD6M .ssh % atom id_rsa_1.pub
 * vikramkrishnan@C02D75HAMD6M .ssh % atom config
 * vikramkrishnan@C02D75HAMD6M .ssh % touch config
 * vikramkrishnan@C02D75HAMD6M .ssh % ssh-add -K ~/.ssh/id_rsa_1
 * Identity added: /Users/vikramkrishnan/.ssh/id_rsa_1 (/Users/vikramkrishnan/.ssh/id_rsa_1)
 * vikramkrishnan@C02D75HAMD6M .ssh % touch config
 *  WE THEN RETURNED THINGS TO THE ORIGINAL CONFIG
 * vikramkrishnan@C02D75HAMD6M .ssh % ssh-add -K ~/.ssh/id_rsa
 * Identity added: /Users/vikramkrishnan/.ssh/id_rsa (vikramkrishnan@C02D75HAMD6M)
 * vikramkrishnan@C02D75HAMD6M .ssh %
 */

object SshAttempt extends App {
  val REMOTE_URL = "git@github.com:vikramkrishnan9885/jsonnet-test.git"

  val localPath = new File("/Users/vikramkrishnan/Desktop/path/directory2")
  if (!localPath.exists) {
    localPath.mkdirs
  }

  val cloneCommand = Git.cloneRepository()
  cloneCommand.setURI(REMOTE_URL)
  cloneCommand.setDirectory(localPath)

  val sshSessionFactory: SshSessionFactory = new JschConfigSessionFactory {
    override def createDefaultJSch(fs: FS): JSch = {
      val defaultJSch: JSch = super.createDefaultJSch(fs)
      defaultJSch.addIdentity("~/.ssh/id_rsa_1")
      defaultJSch
    }

    override def configure(host: OpenSshConfig.Host , session: Session ): Unit = {
      session.setConfig("StrictHostKeyChecking", "false")
    }
  }

  cloneCommand.setTransportConfigCallback(new TransportConfigCallback() {
    override def configure(transport: Transport): Unit = {
      val sshTransport: SshTransport = transport.asInstanceOf[SshTransport]
        sshTransport.setSshSessionFactory(sshSessionFactory)
    }
  })

  cloneCommand.call()
}
