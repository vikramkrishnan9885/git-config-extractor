package gitBits

import java.io.File

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider

object Puller extends App {

  val repo: Repository = new FileRepositoryBuilder()
    // NOTE THE .git
    .setGitDir(new File("/Users/vikramkrishnan/Desktop/path/directory/.git"))
    .build();

  val git = new Git(repo)

  val cp = new UsernamePasswordCredentialsProvider("vikramkrishnan9885", "SH@221bBS")

  val pull = git.pull
    .setCredentialsProvider(cp)
    .setRemote("origin")
    .setRemoteBranchName("master")
    .call

}
