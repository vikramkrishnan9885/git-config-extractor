package gitBits

import java.io.File

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider

object Main extends App {

  val REMOTE_URL = "https://gitlab.com/vikramkrishnan9885/logging-k8s.git"

  val localPath = new File("/Users/vikramkrishnan/Desktop/path/directory")
  if (!localPath.exists) {
    localPath.mkdirs
  }

  val cp = new UsernamePasswordCredentialsProvider("vikramkrishnan9885", "xxx")

  val git = Git.cloneRepository()
    .setURI(REMOTE_URL)
    .setCredentialsProvider(cp)
    .setDirectory(localPath)
    .call()

  val pull = git.pull
    .setCredentialsProvider(cp)
    .setRemote("origin")
    .setRemoteBranchName("master")
    .call

}
