Last login: Wed Dec  9 12:38:58 on ttys000
vikramkrishnan@C02D75HAMD6M ~ % cd ~/.ssh
vikramkrishnan@C02D75HAMD6M .ssh % ls
config		id_rsa		id_rsa.pub	known_hosts
vikramkrishnan@C02D75HAMD6M .ssh % atom id_rsa
vikramkrishnan@C02D75HAMD6M .ssh % ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/vikramkrishnan/.ssh/id_rsa):
/Users/vikramkrishnan/.ssh/id_rsa already exists.
Overwrite (y/n)? ^C
vikramkrishnan@C02D75HAMD6M .ssh % ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/vikramkrishnan/.ssh/id_rsa): id_rsa_1
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in id_rsa_1.
Your public key has been saved in id_rsa_1.pub.
The key fingerprint is:
SHA256:JO1pwCWhlL45cwnWH4cnhKTaQJL2nH1oInkx9nRmpvQ vikramkrishnan@C02D75HAMD6M
The key's randomart image is:
+---[RSA 3072]----+
|... .o+o.        |
|.+ =.*.O.        |
|. *.@.%.o.       |
| o X+*.E+.o      |
|  +.++.oS=       |
|    = o..        |
|     +           |
|                 |
|                 |
+----[SHA256]-----+
vikramkrishnan@C02D75HAMD6M .ssh % atom id_rsa_1
vikramkrishnan@C02D75HAMD6M .ssh % ssh-keygen -t rsa -m PEM
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/vikramkrishnan/.ssh/id_rsa): id_rsa_1
id_rsa_1 already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in id_rsa_1.
Your public key has been saved in id_rsa_1.pub.
The key fingerprint is:
SHA256:rw5QsUC6ySLaaYPAxGq3XlyIkVI/vcU2bwEgO4mKzVo vikramkrishnan@C02D75HAMD6M
The key's randomart image is:
+---[RSA 3072]----+
|   oo.....       |
|. ..+.=o. .      |
| +.+ *o. = .     |
|=+ooo.+ + o .    |
|*+Eo.. oS  o     |
|=*..o..  ..      |
|+ =. o.   .      |
| ....  . .       |
|   .   .o        |
+----[SHA256]-----+
vikramkrishnan@C02D75HAMD6M .ssh % atom id_rsa_1
vikramkrishnan@C02D75HAMD6M .ssh % atom id_rsa_1.pub
vikramkrishnan@C02D75HAMD6M .ssh % atom config
vikramkrishnan@C02D75HAMD6M .ssh % touch config
vikramkrishnan@C02D75HAMD6M .ssh % ssh-add -K ~/.ssh/id_rsa_1
Identity added: /Users/vikramkrishnan/.ssh/id_rsa_1 (/Users/vikramkrishnan/.ssh/id_rsa_1)
vikramkrishnan@C02D75HAMD6M .ssh % touch config
vikramkrishnan@C02D75HAMD6M .ssh % ssh-add -K ~/.ssh/id_rsa
Identity added: /Users/vikramkrishnan/.ssh/id_rsa (vikramkrishnan@C02D75HAMD6M)
vikramkrishnan@C02D75HAMD6M .ssh % cd ~/Documents/self-learning/learning-ml
vikramkrishnan@C02D75HAMD6M learning-ml % touch test.txt
vikramkrishnan@C02D75HAMD6M learning-ml % echo "Hello ssh test" >> test.txt
vikramkrishnan@C02D75HAMD6M learning-ml % git add .
vikramkrishnan@C02D75HAMD6M learning-ml % git commit -m "Testing that ssh still works"
[master d8760c87] Testing that ssh still works
 1 file changed, 1 insertion(+)
 create mode 100644 test.txt
vikramkrishnan@C02D75HAMD6M learning-ml % git push origin master
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 12 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 310 bytes | 310.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
To github.com:vikramkrishnan9885/learning-ml.git
   69d9c407..d8760c87  master -> master
vikramkrishnan@C02D75HAMD6M learning-ml %
