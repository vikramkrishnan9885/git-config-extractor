package gitBits

import java.io.File
import java.util.TimerTask

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider

class TimedPuller(name: String) extends TimerTask{
  override def run(): Unit = {

    println(Thread.currentThread().getName + " beginning to execute ...")

    val repo:Repository = new FileRepositoryBuilder()
      // NOTE THE .git
      .setGitDir(new File("/Users/vikramkrishnan/Desktop/path/directory/.git"))
      .build()

    val git = new Git(repo)

    val cp = new UsernamePasswordCredentialsProvider("vikramkrishnan9885","xxx")

    val pull = git.pull
      .setCredentialsProvider(cp)
      .setRemote("origin")
      .setRemoteBranchName("master")
      .call
  }
}
