package gitBits

import java.util.Timer

object ExecuteTimer extends App {

  val te = new TimedPuller("pull")

  val t = new Timer()
  t.scheduleAtFixedRate(te, 1000, 50 * 1000)

}
