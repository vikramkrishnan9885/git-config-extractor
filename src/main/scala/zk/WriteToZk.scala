package zk

import java.nio.file.{Files, Paths}

import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.zookeeper.CreateMode
import org.apache.zookeeper.data.Stat

// https://loneidealist.medium.com/apache-curator-in-5-minutes-8a2f91aff06f
// https://learning.oreilly.com/library/view/apache-zookeeper-essentials/9781784391324/ch06.html
// https://blog.knoldus.com/how-to-setup-and-use-zookeeper-in-scala-using-apache-curator/

object WriteToZk extends App {

  val file1 = "/Users/vikramkrishnan/Desktop/path/directory/sample.yml"

  val bytes: Array[Byte] = Files.readAllBytes(
    Paths.get(file1)
  )

  val retryPolicy = new ExponentialBackoffRetry(1000, 3)
  val curatorZookeeperClient = CuratorFrameworkFactory.newClient("localhost:2181", retryPolicy)
  curatorZookeeperClient.start
  curatorZookeeperClient.getZookeeperClient.blockUntilConnectedOrTimedOut

  // Creating a ZNode
  val znodePath = "/test_node"

  // https://stackoverflow.com/questions/12537516/how-do-i-check-if-a-path-exists-in-zookeeper-using-curator
  //val nodeExists: Stat = curatorZookeeperClient.checkExists().forPath("/abcd")
  val nodeExists: Stat = curatorZookeeperClient.checkExists().forPath(znodePath)
  println(nodeExists)

  /**
  val async = AsyncCuratorFramework.wrap(curatorZookeeperClient)

  val exists = new AtomicBoolean(false);

  val x: CompletionStage[Void] = async.checkExists()
    .forPath("/test_node")
    .thenAcceptAsync(s => exists.set(s != null))

  val y = exists.get()

  println(y)
  */


    if(nodeExists == null) {
      val path = curatorZookeeperClient.create()
        .creatingParentsIfNeeded()
        .withMode(CreateMode.PERSISTENT)
        .forPath(znodePath, "some_text".getBytes()) // MAYBE WE DON'T WRITE TO NODE HERE. SEE BELOW
      /**
       * Creating a ZNode
       * client.create().withMode(CreateMode.PERSISTENT).forPath("/your/ZNode/path");
       * Here, the CreateMode specify what type of a node you want to create. Available types are PERSISTENT,EPHEMERAL,EPHEMERAL_SEQUENTIAL,PERSISTENT_SEQUENTIAL,CONTAINER.
       */
    }

  // If you are doing it later, (data should be given as a byte array)
  curatorZookeeperClient.setData().forPath(znodePath,bytes)

  // If you want to retrive data in a given path,
  val ba: Array[Byte] = curatorZookeeperClient.getData().forPath(znodePath)
  val s = new String(ba)
  println(s)
}
