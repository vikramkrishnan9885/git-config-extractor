package cli

import java.io.{BufferedReader, File, FileInputStream, InputStreamReader}

import org.apache.commons.io.IOUtils

import scala.Console.out

object CliRun extends App {

  val fis: FileInputStream = new FileInputStream("scripts/script.sh");
  val data: String = IOUtils.toString(fis, "UTF-8");
  println(data)

  /**
  //Run macro on target
  val pb: ProcessBuilder = new ProcessBuilder(data);
  //pb.directory(new File("./scripts/"));
  pb.redirectErrorStream(true);
  val process: Process = pb.start();

  //Read output
  val out: StringBuilder = new StringBuilder();
  val br = new BufferedReader(new InputStreamReader(process.getInputStream()));
  var line: String  = null
  var previous: String = null
  while ((line = br.readLine()) != null)
    if (!line.equals(previous)) {
      previous = line;
      out.append(line).append('\n');
      System.out.println(line);
    }

  //Check result
  if (process.waitFor() == 0) {
    System.out.println("Success!");
    System.exit(0);
  }

  //Abnormal termination: Log command parameters and output and throw ExecutionException
  System.err.println(data);
  System.err.println(out.toString());
  System.exit(1);
  */
}
