package cli

import java.io.{BufferedReader, File, InputStreamReader}

object CliRun2 extends App {

  val pb: ProcessBuilder = new ProcessBuilder("ls")
  pb.directory(new File("/Users/vikramkrishnan/Desktop"))
  pb.redirectErrorStream(true)
  val p = pb.start()

  val out = new StringBuilder
  val br = new BufferedReader(new InputStreamReader(p.getInputStream()));
  var line: String = null
  var previous: String = null;
  while ((line = br.readLine()) != null)
    if (!line.equals(previous)) {
      previous = line;
      out.append(line).append('\n');
      System.out.println(line);
    }

  //Check result
  if (p.waitFor() == 0) {
    System.out.println("Success!");
    System.exit(0);
  }

}
