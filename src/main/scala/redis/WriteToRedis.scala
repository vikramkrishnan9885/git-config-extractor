package redis

import java.nio.file.{Files, Paths}

import redis.clients.jedis.Jedis

object WriteToRedis extends App {

  val host: String = "localhost"
  val port: Int = 6379

  val file1 = "/Users/vikramkrishnan/Desktop/path/directory/sample.yml"

  val bytes: Array[Byte] = Files.readAllBytes(
    Paths.get(file1)
  )

  val key: Array[Byte] = "test_key".getBytes()

  val jedis = new Jedis(host, port)

  jedis.set(key, bytes)

  val retrieved = jedis.get(key)
  println(new String(retrieved))
  jedis.close()
}
