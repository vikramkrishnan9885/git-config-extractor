package etcd

import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.CompletableFuture

import io.etcd.jetcd.kv.GetResponse
import io.etcd.jetcd.{ByteSequence, Client}

import com.google.common.base.Charsets.UTF_8

object WriteToEtcd extends App {

  val file0 = "/Users/vikramkrishnan/Desktop/path/directory/application.properties"
  val file1 = "/Users/vikramkrishnan/Desktop/path/directory/sample.yml"
  val file2 = "/Users/vikramkrishnan/Desktop/path/directory/lorem-ipsum.txt"

  val array = Files.readAllBytes(
    Paths.get(file1)
  )
  println(array)


  // create client
  val client = Client.builder
    .endpoints("http://localhost:2379")
    .build

  val kvClient = client.getKVClient()

  val keyBytes: Array[Byte] = "test_key".getBytes()

  // KEY AND FOLDER STRUCTURE SHOULD BE:
  // TEAM
  // PROJECT
  // ENV
  // VERSION ???
  // NAME
  val key: ByteSequence = ByteSequence.from(keyBytes)
  private val value = ByteSequence.from(array)

  // put the key-value
  kvClient.put(key, value).get() // .get() is because of Futures

  val getFuture: CompletableFuture[GetResponse] = kvClient.get(key);

  // get the value from CompletableFuture
  val response: GetResponse = getFuture.get()
  //println(response)

  // https://github.com/etcd-io/jetcd/blob/master/jetcd-examples/jetcd-ctl/src/main/java/io/etcd/jetcd/examples/ctl/CommandGet.java
  println(response.getKvs.get(0).getValue().toString(UTF_8))

}
