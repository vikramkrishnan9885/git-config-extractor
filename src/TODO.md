INPUTS:
=======
* Repo
* branch to pull
* Timer period for branch pull
* Directory to save to
* SSH vs credentials
* If ssh then passphrase
* Username and password (Env var that is passed as input)
