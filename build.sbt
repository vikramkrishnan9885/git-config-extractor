name := "GitConfigExtractor"

version := "0.1"

scalaVersion := "2.13.4"

// https://mvnrepository.com/artifact/org.eclipse.jgit/org.eclipse.jgit
libraryDependencies += "org.eclipse.jgit" % "org.eclipse.jgit" % "5.9.0.202009080501-r"
// https://mvnrepository.com/artifact/org.eclipse.jgit/org.eclipse.jgit.ssh.jsch
libraryDependencies += "org.eclipse.jgit" % "org.eclipse.jgit.ssh.jsch" % "5.9.0.202009080501-r"


// https://mvnrepository.com/artifact/io.etcd/jetcd-core
libraryDependencies += "io.etcd" % "jetcd-core" % "0.3.0"

// https://mvnrepository.com/artifact/commons-io/commons-io
libraryDependencies += "commons-io" % "commons-io" % "2.7"

// https://mvnrepository.com/artifact/com.jcraft/jsch
libraryDependencies += "com.jcraft" % "jsch" % "0.1.55"

// https://mvnrepository.com/artifact/redis.clients/jedis
libraryDependencies += "redis.clients" % "jedis" % "3.3.0"

// https://mvnrepository.com/artifact/org.apache.curator/curator-framework
libraryDependencies += "org.apache.curator" % "curator-framework" % "5.1.0"

// https://mvnrepository.com/artifact/org.apache.curator/curator-recipes
libraryDependencies += "org.apache.curator" % "curator-recipes" % "5.1.0"

// https://mvnrepository.com/artifact/org.apache.curator/curator-x-async
libraryDependencies += "org.apache.curator" % "curator-x-async" % "5.1.0"


